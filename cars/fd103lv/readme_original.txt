================================================================

Car Name                : Dodge Viper GTS-R Concept 

installation directory  : revolt/cars/vipergtsr

Author                  : Major Clod

Email Address           : veen01@powerup.com.au


Car description         : My, first real racing car for re-volt, it is the new Dogde Viper GTS-R 			  2000 Concept.  I have spend a long time working on this car, trying to 			  get the mesh, textures and performance right.  This car goes about 			          44mph, has decent accelleration and handling.  I did not want to make 			  it a supercar.  I hope you have as much fun racing with this one as I 			  did making it.  

Class			: Pro

Engine			: Glow

Other info              : Made from scratch using 5 pictures from www.supercars.net.  Also 			          includes custom Y-spoke wheels.  Some of the Car "Gloss" is distorted 			  from some angles, especially at the start of the Garden track.  I tried 			  for ages to try and fix this, but I had no luck.  If anyone knows how 			  to fix this please email me


Additional Credits to   : Acclaim for making an awesome game.  

		

================================================================


* How to use this car *

Unzip all files to revolt/cars/vipergtsr


* Copyright / Permissions *

This car is copyright (c)1999 Major Clod.  You cannot distribute or sell this commercially without my written permission.  You may not distribute any modifications to this product without obtaining my permission.  All you have to do is email me.


