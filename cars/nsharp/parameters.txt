{

;============================================================
;============================================================
; Marussia
;============================================================
;============================================================
Name       	"N-Sharp"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	4 			; Skill level (rookie, amateur, ...)
TopEnd     	3669.364502 		; Actual top speed (mph) for frontend bars
Acc        	5.483109		; Acceleration rating (empirical)
Weight     	1.400000 		; Scaled weight (for frontend bars)
Trans      	2 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 		; Max Revs (for rev counter, deprecated...)

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/nsharp/body.prm"
MODEL 	1 	"cars/nsharp/wheelfl.prm"
MODEL 	2 	"cars/nsharp/wheelfr.prm"
MODEL 	3 	"cars/nsharp/wheelbl.prm"
MODEL 	4 	"cars/nsharp/wheelbr.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 		"cars/nsharp/hull.hul"
TPAGE 		"cars/nsharp/car.bmp"
;)SFXENGINE 	"cars/nsharp/sound_002.wav"
;)TCARBOX 	"cars/nsharp/carbox.bmp" 			
;)TSHADOW 	"cars/nsharp/shadow.bmp" 			
;)SHADOWINDEX 	-1 							
;)SHADOWTABLE 	-74.0 74.0 71.0 -73.0 -4.1
;)SFXSERVO 	"NONE"
;)SFXHONK 	"NONE"
EnvRGB 		100 100 100

;====================
; Handling related stuff
;====================

;)Statistics	TRUE
SteerRate  	4.650000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.850000 			; Rate at which Engine voltage approaches set value
TopSpeed   	39.000000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -2.000000 4.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset
;)Flippable	false 		; Rotor car effect
;)Flying   	false 		; Flying like the UFO car
;)ClothFx  	false 		; Mystery car cloth effect

;====================
; Camera details
;====================

;)CAMATTACHED {	; Start Camera
;)HoodOffset   	0.000000 0.000000 0.000000 	; Offset from model center
;)HoodLook     	0.050000 			; Look angle (-0.25 to 0.25, 0.0 - straight ahead)
;)RearOffset   	0.000000 0.000000 0.000000
;)RearLook     	0.050000
;)FixedOffset  	true 				; Is offset fixed or moving
;)FixedLook    	true 				; Is look fixed or moving
;)UseDefault   	true 				; Use default offsets (computed in game)
;)}            	; End Camera

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.400000
Inertia    	1600.000000 0.000000 0.000000
           	0.000000 1950.000000 0.000000
           	0.000000 0.000000 799.436579
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.002000 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.790000
KineticFriction 0.430000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-25.000000 1.000000 34.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.300000
EngineRatio 	13000.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	10.000000
ToeIn       	0.000000
AxleFriction    	0.025000
Grip            	0.015000
StaticFriction  	2.000000
KineticFriction 	1.80000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	25.000000 1.000000 34.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.300000
EngineRatio 	13000.000000
Radius      	10.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	10.000000
ToeIn       	0.000000
AxleFriction    	0.025000
Grip            	0.015000
StaticFriction  	2.000000
KineticFriction 	1.80000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-24.500000 0.000000 -42.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	1.000000
EngineRatio 	30000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	11.000000
ToeIn       	0.000000
AxleFriction    	0.045000
Grip            	0.015000
StaticFriction  	2.000000
KineticFriction 	2.000000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	24.500000 0.000000 -42.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	1.000000
EngineRatio 	30000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	10.000000
ToeIn       	0.000000
AxleFriction    	0.045000
Grip            	0.015000
StaticFriction  	2.000000
KineticFriction 	2.000000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.710000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.710000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	850.000000
Damping     	13.000000
Restitution 	-0.730000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	850.000000
Damping     	13.000000
Restitution 	-0.730000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	18.000000 -17.000000 -47.000000
Direction   	0.000000 -1.000000 0.000000
Length      	17.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	630.643433
UnderRange  	1094.642822
UnderFront	 	1778.746338
UnderRear   	738.563232
UnderMax    	0.950000
OverThresh  	517.576172
OverRange   	1391.000000
OverMax     	0.962583
OverAccThresh  	348.559540
OverAccRange   	420.339996
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}
