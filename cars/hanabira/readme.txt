Information
-----------------
Car Name:	Hanabira
Rating:		Super Pro
Top speed:	4?.? mph
Acceleration:	?.?? m/s�
Weight:		1.7 kg
Author:		Saffron, Mushy, Skarma


Description
-----------------
A remodel of Skyedge, fine-tuned for a challenging yet rewarding drive in the Super Pro class. With extremely agile four-wheel steering, high acceleration and above average top speed, the results this car gets is entirely down to who drives it.

In the late 1990s emerged a mysterious individual who would single-handedly change the R/C racing world, with their custom made prototype smashing records on tracks like the Keitune Racing Speedway that wouldn't be matched for another two decades.

The individual modified a the late 90s variant of the Nishida Skyhigh. Donning a pink, innocent paintjob, the prototype - named "Hanabira" - would require someone to be one with this car's ability to flow around any racetrack, like a petal in the wind.

Enthusiasts in the R/C world could never figure out how the prototype had so much success at first, but eventually came to the conclusion that it was down to its extreme four-wheel steering and raw performance - but most importantly, the one controlling it.

This prototype would later prove to be a benchmark for all the major players in the R/C scene, like Toy-Volt, as they begin work on their Toy-Volt ULTRA line.


Requirements
-----------------
You MUST use the latest RVGL patch for the textures to display properly.


Credits
-----------------
Xarc for Skyedge
Mushy for the paintjob
Skarma for minimising Hanabira's collision issues
Trixed for better or worse, the inspiration for the car
The Blender Foundation for Blender
Marv, Martin and Huki for the Blender plug-in
Jigebren for PRM2HUL
The RVGL Team for RVGL
Rick Brewster for Paint.NET