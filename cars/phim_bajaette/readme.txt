==Baj�tte by Phimeek==

This car was inspired after I watched a "virtual tuning" video where a YouTuber named Evotech 5 turned a Corvette into a offroad build using Photoshop.

Base: NY 54
Rating: Advanced
Top Speed: 36 mph / 57 kph
Acceleration: Normal
Handling: A little grippy but can oversteer sometimes too

Programs used: Blender, Inkscape, GIMP
Carbox by Xarc