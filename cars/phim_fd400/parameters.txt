{

;============================================================
;============================================================
; FD-400
;============================================================
;============================================================
Name      	"FD-400"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\phim_fd400\body.prm"
MODEL 	1 	"cars\phim_fd400\wheel-fl.prm"
MODEL 	2 	"cars\phim_fd400\wheel-fr.prm"
MODEL 	3 	"cars\phim_fd400\wheel-bl.prm"
MODEL 	4 	"cars\phim_fd400\wheel-br.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 	"cars\phim_fd400\car.bmp"
COLL 	"cars\phim_fd400\hull.hul"
;)TCARBOX "cars\phim_fd400\box.bmp"
;)TSHADOW "cars\phim_fd400\shadow.bmp"
;)SHADOWTABLE -81.0397 81.0397 92.3805 -69.3805 -0.1809
EnvRGB 	200 200 200

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
;)CPUSelectable TRUE
;)Statistics TRUE
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	5 			; Skill level (rookie, amateur, ...)
TopEnd     	4113.395020 			; Actual top speed (mph) for frontend bars
Acc        	8.674873 			; Acceleration rating (empirical)
Weight     	1.400000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	2 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	5.000000 			; Rate at which Engine voltage approaches set value
TopSpeed   	47.500000 			; Car's theoretical top speed (not including friction...)
DownForceMod	4.000000 			; Down force modifier when car on floor
CoM        	0.000000 0.000000 -10.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.40000
Inertia    	1830.000000 0.000000 0.000000
           	0.000000 2100.000000 0.000000
           	0.000000 0.000000 900.000000
Gravity	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.000800 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.200000 -3.760000 49.840000
Offset2  	-0.500000 0.000000 0.000000
IsPresent   		TRUE
IsPowered   		FALSE
IsTurnable  		TRUE
SteerRatio  		-0.300000
EngineRatio 		4000.000000
Radius      		11.000000
Mass        		0.150000
Gravity     		2200.000000
MaxPos      		4.000000
SkidWidth   		13.000000
ToeIn       		0.000000
AxleFriction    	0.025000
Grip            	0.030000
StaticFriction  	2.150000
KineticFriction 	2.150000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	26.200000 -3.760000 49.8400000
Offset2  	0.500000 0.000000 0.000000
IsPresent   		TRUE
IsPowered   		FALSE
IsTurnable  		TRUE
SteerRatio  		-0.300000
EngineRatio 		4000.000000
Radius      		11.000000
Mass        		0.150000
Gravity     		2200.000000
MaxPos      		4.000000
SkidWidth   		13.000000
ToeIn       		0.000000
AxleFriction    	0.025000
Grip            	0.030000
StaticFriction  	2.150000
KineticFriction 	2.150000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-25.200000 -4.600000 -34.830000
Offset2  	-0.500000 0.000000 0.000000
IsPresent   		TRUE
IsPowered   		TRUE
IsTurnable  		FALSE
SteerRatio  		1.000000
EngineRatio 		19500.000000
Radius      		11.000000
Mass        		0.150000
Gravity     		2200.000000
MaxPos      		4.000000
SkidWidth   		13.000000
ToeIn       		0.000000
AxleFriction    	0.050000
Grip            	0.030000
StaticFriction  	2.270000
KineticFriction 	2.270000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	25.200000 -4.600000 -34.830000
Offset2  	0.500000 0.000000 0.000000
IsPresent   		TRUE
IsPowered   		TRUE
IsTurnable  		FALSE
SteerRatio  		1.000000
EngineRatio 		19500.000000
Radius      		11.000000
Mass        		0.150000
Gravity     		2200.000000
MaxPos      		4.000000
SkidWidth   		13.000000
ToeIn       		0.000000
AxleFriction    	0.050000
Grip            	0.030000
StaticFriction  	2.270000
KineticFriction 	2.270000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	26.020000 -19.810000 44.390000
Direction   	0.000000 -1.000000 0.000000
Length      	22.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	1000.000000
UnderRange  	1000.000000
UnderFront	 1000.000000
UnderRear   	1000.000000
UnderMax    	0.680000
OverThresh  	80.000000
OverRange   	500.000000
OverMax     	0.900000
OverAccThresh  70.000000
OverAccRange   50.000000
PickupBias     3276
BlockBias      3276
OvertakeBias   16383
Suspension     0
Aggression     0
}           	; End AI

}

Acclaim GT
