August 06, 2017: Readme file of 75C.

================================================================
======================== Car Info ==============================
================================================================

Name				75C
Author				Allan1
Date				December 02, 2011
Type				Remodel
Folder Name			corv75
Rating				Semi-Pro
Class				Glow
Drivetrain			Rear-wheel Drive
Top speed			35 mph
Body mass			1.2 kg


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory.


================================================================
======================= Description ============================
================================================================

That's a remodel of Sprinter XL. The paintjob and shape were based on a Hot Wheels car called 75 Corvette. Parameters are quite soft.


================================================================
======================== Credits ===============================
================================================================

Sprinter XL was used for this creation, which is a stock car made by the game creators.

Credits for base design comes to Hot Wheels.

Track in the preview picture is 12-21-12 by RickyD.

Tools:
Autodesk 3d Studio Max 2010
Asetools (by Ali and Kay)
getUV (by Kay)
Adobe Photoshop CS2
MS Paint
Re-Volt 1.2 (By Huki and Jig)
CorelDRAW x3
Prm2Hul (By Jig)
Blender 2.66a
MS Notepad


================================================================
======================= Changelog ==============================
================================================================

July 13, 2016:
-Remodeled and remaped the body
-Improved graphics
-Fixed parameters
-Added carbox and shadow

June 07, 2017:
-Fixed axles


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan