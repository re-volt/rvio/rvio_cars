================================================================
Car name                : Fnone   
Install in folder       : Re-volt\cars\fnone
Author                  : buba z/Rex R
Email Address           : rexc.reynolds@frontier.com
Misc. Author Info       : 

Description             : more or less based on a honda indy/formula
	race car

Additional  notes       : this car was started before the Eaglet
buba did the mesh work, preliminary paint,and parameters(including
the fiddely bits of locating the wheels etc.) my involvement was on
the order of supplying some refinements to the mapping and tuning
the parameters 
================================================================

* Play Information *

Top speed (observed)    : 37/38 after a long, long straight
Rating                  : semi-pro
* Construction *

Base                    : New Car from scratch
Editor(s) used          : 3ds max,psp,zmodeler,neopaint,notepad,
(rv)shade,dump
Known Bugs              : small ones if any


* Copyright / Permissions *

Authors (MAY) use this Car as a base to build additional
cars.  



You MAY distribute this CAR, provided you include this file, with
no modifications.  You may distribute this file in any electronic
format (BBS, Diskette, CD, etc) as long as you include this file 
intact.



* Where to get this CAR *

where you found this file