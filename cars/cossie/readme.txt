Information
-----------------
Car Name:	Cossie
Rating:		Semi-Pro
Top speed:	37.3 mph
Acceleration:	2.75 m/s�
Weight:		1.7 kg
Author:		Saffron


Description
-----------------
A stocklike 1992 Ford Escord Cosworth, with two paintjobs.


Requirements
-----------------
You MUST use the latest RVGL patch for the textures to display properly.


Credits
-----------------
The Blender Foundation for Blender
Marv, Martin and Huki for the Blender plug-in
Jigebren for PRM2HUL
Phimeek for the decals
Whoever made the wheel texture, I cannot remember
The RVGL Team for RVGL
Rick Brewster for Paint.NET


Permission
-----------------
Make sure to credit me for whatever you end up doing with or based off this car.