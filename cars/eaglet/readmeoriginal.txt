================================================================
Car name                : Indy Eaglet   
Install in folder       : Re-volt\cars\indye
Author                  : Rex Reynolds
Email Address           : rex.reynolds@worldnet.att.net
Misc. Author Info       : 

Description             : inspired by the 1968 winner of the indy 500 and by the Hot Wheels
car "Indy Eagle":this car meets the O.R.C.A standard

Additional Credits to   :Bubba Z for doing the model work(I sent him the plans and the result
you see here);snow tiger did the wheels 
================================================================

* Play Information *

Top speed (observed)    :33 mph
Rating                  : Advanced
* Construction *

Base                    : New Car from scratch
Editor(s) used          :Neopaint, 3-d studio Max v 2.5, MS Notepad

Known Bugs              :do not try to edit the parameters with car manager ver 1.3
this will cause nasty things to happen, some handling problems on nhood1, some skin mapping
glitches

Additional notes        :car makers(modelers) if you would like to try your hand at modeling
 this car drop me a line and I will send you the same delvelopment package that I sent
  Bubba Z
* Copyright / Permissions *

Authors (MAY) use this Car as a base to build additional
cars.  proviso: give us credit for our work

(One of the following)

You MAY distribute this CAR, provided you include this file, with
no modifications.  You may distribute this file in any electronic
format (BBS, Diskette, CD, etc) as long as you include this file 
intact.




