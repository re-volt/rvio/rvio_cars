﻿04:32 18/02/2016

	~ ~ ~ ~ ~ ~ ~ ~ ~ ~ SKARMINATER　プレゼント ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

		        Collaboration with Marv

	     	               <Riptor>

＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Preface)

...
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Car Information)

Date	  : 01/02/2014 - 18/02/2016
Car Name  : Riptor
Car Type  : Conversion
Folder    : ...\cars\riptor
Install   : As with any custom car, just drag the cars folder into your Re-Volt folder.
Top Speed : 38.6 MPH (observed)
Rating    : Semi-Pro
Car #     : 582
Release # : 313

Apparently I never numbered this car when it was first converted so I've had to put it at the end, hence the latest release number conflicting with the date. This has been a long time awaiting to be released. It's the first proper RC car I've actually made for RV. Shock! Wow! I actually made an ACTUAL RC CAR.

...Anyway, there were many ups and downs this car went through before finally getting released. Having to deal with the axles, scaling and the final testing. The feedback I was given said that the car was too big and needed a few adjustments, but considering that it's around the same size as Bigvolt and Bossvolt, albeit possibly a bit wider, I'm just going to release it as is.

I tried to make it fit in and balanced the parameters with the other stock cars as best I could. It has very quick acceleration and a middling top speed to compensate for it's large size. Rather suited for jumps and stunts, I just hope that it actually lives up to the expectation. Feel free to repaint it and whatnot and, as always, enjoy it!
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Construction)

Base           : Original model & textures for Stunt Cars on WiiWare by BrightRetro, remapped for RV by Marv
Editor(s) used : Blender 2.74 + Jigebren's RV Plugin, Car:Load, PRM2HULL, Paint.net, Notepad
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Thanks)

BrightRetro for the creation of and providing the original model, Marv for remapping it for Re-Volt.
Killer Wheels for the Slippery Slope track where I took the picture.
hilaire9 for his huge selection of tracks.
Zach/ZAgames for hosting RVZ.
KDL for the Car:Load and CarLighter tools.
Jigebren for his PRM2HULL tool and Blender plugin.
ZR, Nero, UR, Marv, Burner94, Mezza, Mmud & the gentlemen in #ReVolt-Chat. Because I say so.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
(Copyrights)

You may do whatever you like with this car providing you keep it in a presentable and respectable manner.

However, I will not appreciate seeing this car scaled back to 1207 standards unless it is for personal use. There is no reason for anyone to do this now anyway seeing as I'm not going to be stingy anymore. But still... Please don't do it.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
(Where to find this car)

Skarma's Derelict Corner - My own website where you can find all of my cars and more.

This car will also be uploaded to RVZ. It may also be found on other sites such as RVA, ARM and Last_Cuban's XTG website.

This car may be uploaded to any of the above sites without consent. PLEASE ask me if you would like to upload it anywhere else.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿

Visit my website, Skarma's Derelict Corner at http://bit.ly/skarma or http://bit.ly/skarmadc
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Disclaimer)

By downloading this car, you are using it at your own risk & discretion. I am not responsible for ANY problems that this car may cause to your computer.

This car SHOULD still work for you no matter what version you're using (bar 1.0). I still recommend that you at least have the 1102 version of the 1.2 patch to fully enjoy this car the way it was intended to be.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
Have fun, etc...