{

;============================================================
;============================================================
; Middle5
;============================================================
;============================================================
Name      	"Middle5"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\middle5\body.prm"
MODEL 	1 	"cars\middle5\wheel-l.prm"
MODEL 	2 	"cars\middle5\wheel-r.prm"
MODEL 	3 	"cars\middle5\axle.prm"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 	"cars\middle5\car.bmp"
TSHADOW "cars\middle5\shadow.bmp"
;)TCARBOX "cars\middle5\carbox.bmp"
;)SHADOWINDEX 	-1
;)SHADOWTABLE -54.5 54.5 77.5 -76.2 -7.8
COLL 	"cars\middle5\hull.hul"
EnvRGB 	200 200 200

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	1 			; Obtain method
Rating     	3 			; Skill level (rookie, amateur, ...)
TopEnd     	3375.231201 			; Actual top speed (mph) for frontend bars
Acc        	7.580225 			; Acceleration rating (empirical)
Weight     	2.200000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	0 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)
;)Statistics TRUE
;)CPUSelectable	TRUE

;====================
; Handling related stuff
;====================

SteerRate  	2.80000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	40.2			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -3.000000 4.00000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	2.200000
Inertia    	1700.000000 0.000000 0.000000
           	0.000000 1900.000000 0.000000
           	0.000000 0.000000 1500.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-20.25 2 40
Offset2  	-4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.250000
EngineRatio 	17400
Radius      	13.50000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	8.000000
SkidWidth   	14.5000
ToeIn       	0.000000
AxleFriction    	0.02000
Grip            	0.012000
StaticFriction  	2.20000
KineticFriction 	2.2000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	20.25 2 40
Offset2  	4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.250000
EngineRatio 	17400
Radius      	13.50000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	8.000000
SkidWidth   	14.5000
ToeIn       	0.000000
AxleFriction    	0.02000
Grip            	0.012000
StaticFriction  	2.20000
KineticFriction 	2.2000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-20.25 2 -41.25
Offset2  	-4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.100000
EngineRatio 	17400
Radius      	13.50000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	8.000000
SkidWidth   	14.5000
ToeIn       	0.000000
AxleFriction    	0.05000
Grip            	0.01700
StaticFriction  	2.4000
KineticFriction 	2.1000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	20.25 2 -41.25
Offset2  	4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.100000
EngineRatio 	17400
Radius      	13.50000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	8.000000
SkidWidth   	14.5000
ToeIn       	0.000000
AxleFriction    	0.05000
Grip            	0.01700
StaticFriction  	2.4000
KineticFriction 	2.1000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	250.000000
Damping     	10.000000
Restitution 	-0.70000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	250.000000
Damping     	10.000000
Restitution 	-0.70000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	250.000000
Damping     	10.000000
Restitution 	-0.70000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	250.000000
Damping     	10.000000
Restitution 	-0.700000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	3
Offset      	-8.09 -3.25 40.70
Length      	10
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	3
Offset      	8.09 -3.25 40.70
Length      	10
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	3
Offset      	-9.45 -7.9 -46.48
Length      	10
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	3
Offset      	9.45 -7.9 -46.48
Length      	10
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	20 -10 -6
Direction   	0.000000 -1.000000 -0.200000
Length      	15.000000
Stiffness   	2500.000000
Damping     	10.00000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	1043.510742
UnderRange  	2702.099854
UnderFront	 	2036.469604
UnderRear   	335.000000
UnderMax    	0.638043
OverThresh  	531.036438
OverRange   	1827.135498
OverMax     	0.640206
OverAccThresh  	613.148560
OverAccRange   	1021.179871
PickupBias     	3276
BlockBias      	3276
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

93B162D7