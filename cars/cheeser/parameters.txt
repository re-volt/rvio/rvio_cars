{

;============================================================
;============================================================
; Swizz Cheeser
;============================================================
;============================================================
Name      	"Swizz Cheeser"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\cheeser\body.prm"
MODEL 	1 	"cars\cheeser\wheelfl.prm"
MODEL 	2 	"cars\cheeser\wheelfr.prm"
MODEL 	3 	"cars\cheeser\wheelbl.prm"
MODEL 	4 	"cars\cheeser\wheelbr.prm"
MODEL 	5 	"cars\cheeser\bone.prm"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"cars\cheeser\axlefl.prm"
MODEL 	9 	"cars\cheeser\axlefr.prm"
MODEL 	10 	"cars\cheeser\axlebl.prm"
MODEL 	11 	"cars\cheeser\axlebr.prm"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\cheeser\AerialT.m"
TPAGE 		"cars\cheeser\car.bmp"
COLL 		"cars\cheeser\hull.hul"
TCARBOX		"cars\cheeser\carbox.bmp"
TSHADOW		"cars\cheeser\shadow.bmp"
SHADOWTABLE	-58.0 58.0 65.0 -53.0 -2.0
EnvRGB 	100 100 100

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
Statistics 	TRUE
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	2 			; Skill level (rookie, amateur, ...)
TopEnd     	3188.887451 			; Actual top speed (mph) for frontend bars
Acc        	5.749380 			; Acceleration rating (empirical)
Weight     	1.100000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	2 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	2.400000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	35.000000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -1.000000 -2.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.100000
Inertia    	780.000000 0.000000 0.000000
           	0.000000 980.000000 0.000000
           	0.000000 0.000000 300.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-25.000000 5.000000 42.900000
Offset2  	-4.500000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.450000
EngineRatio 	16000.000000
Radius      	10.700000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	9.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.013000
StaticFriction  	1.780000
KineticFriction 	1.580000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	25.000000 5.000000 42.900000
Offset2  	4.500000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.450000
EngineRatio 	16000.000000
Radius      	10.700000
Mass        	0.200000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	9.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.013000
StaticFriction  	1.780000
KineticFriction 	1.580000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-22.000000 7.500000 -32.550000
Offset2  	-6.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.100000
EngineRatio 	16000.000000
Radius      	11.200000
Mass        	0.210000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.013000
StaticFriction  	1.790000
KineticFriction 	1.590000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	22.000000 7.500000 -32.550000
Offset2  	6.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.100000
EngineRatio 	16000.000000
Radius      	11.200000
Mass        	0.210000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.013000
StaticFriction  	1.790000
KineticFriction 	1.590000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	350.000000
Damping     	7.000000
Restitution 	-0.800000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	350.000000
Damping     	7.000000
Restitution 	-0.800000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	5
Offset      	-4.900000 3.300000 -31.800000
Length      	17.000000
Stiffness   	360.000000
Damping     	8.000000
Restitution 	-0.800000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	5
Offset      	4.900000 3.300000 -31.800000
Length      	17.000000
Stiffness   	360.000000
Damping     	8.000000
Restitution 	-0.800000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	8
Offset      	-7.500000 3.700000 42.900000
Length      	16.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	9
Offset      	7.500000 3.700000 42.900000
Length      	16.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	10
Offset      	-9.910000 3.750000 -20.450000
Length      	17.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	11
Offset      	9.910000 3.750000 -20.450000
Length      	17.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	11.300000 -4.500000 -18.000000
Direction   	0.000000 -1.000000 0.000000
Length      	24.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	5.000000
UnderRange  	1584.415039
UnderFront	 	401.940002
UnderRear   	321.519989
UnderMax    	0.283685
OverThresh  	1954.575073
OverRange   	1541.581543
OverMax     	1.000000
OverAccThresh  	131.029999
OverAccRange   	1928.010010
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	26213
Aggression     	0
}           	; End AI

}

3D8A0499